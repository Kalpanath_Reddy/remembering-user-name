import { LightningElement, api, track, wire } from "lwc";
export default class temp extends LightningElement {

setCookie() {
        var d = new Date();
        d.setTime(d.getTime() + (365 * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        var cname = "username=" + this.username;
        document.cookie = cname;
        document.cookie = expires;
        document.cookie = "path=/";
    }

    getcooks() {
        var name = "username=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
}